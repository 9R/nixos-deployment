{
  lib,
  config,
  ...
}: let
  cfg = config.muccc.qemu-guest;
  onProxmox = config.networking.domain == "club.muc.ccc.de";
in {
  options = {
    muccc.qemu-guest = {
      enable = lib.mkEnableOption "qemu guest profile";
    };
  };

  config = lib.mkIf cfg.enable {
    boot = {
      initrd = {
        availableKernelModules = ["virtio_net" "virtio_pci" "virtio_mmio" "virtio_blk" "virtio_scsi" "9p" "9pnet_virtio"];
        kernelModules = ["virtio_balloon" "virtio_console" "virtio_rng"];
      };
      kernelParams = ["console=ttyS0"];
      loader = {
        timeout = 3;
        grub.device = "/dev/vda";
      };
    };

    fileSystems = {
      "/" = lib.mkDefault {
        device = "/dev/disk/by-label/nixos";
        fsType = "ext4";
        autoResize = true;
      };
    };

    services.qemuGuest.enable = onProxmox;

    # trimming is generally a good idea for VMs for most backing stores
    services.fstrim = {
      enable = true;
      interval = "weekly";
    };
  };
}
