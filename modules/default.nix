{
  name,
  config,
  pkgs,
  lib,
  flakes,
  ...
}: let
  selfLib = flakes.self.lib;
in {
  imports = [
    flakes.sops-nix.nixosModules.sops
    flakes.mumble-exporter.nixosModules.default
    flakes.home-manager.nixosModules.home-manager
    flakes.authentik.nixosModules.default
  ];

  nixpkgs.overlays = [
    flakes.muccc-api.overlays.default
    flakes.luftschleuse2.overlays.default
  ];

  system.stateVersion = lib.mkDefault "23.05";

  sops = {
    defaultSopsFile = ../secrets + "/${config.networking.hostName}.yaml";
  };

  networking = {
    hostName = lib.mkDefault name;
    domain = lib.mkDefault "club.muc.ccc.de";
    useDHCP = false;
    useNetworkd = true;
    firewall = {
      logRefusedConnections = false;
    };
  };

  time.timeZone = "UTC";
  services.getty.helpLine = lib.mkForce ''
    ip6: \6
    ip4: \4
  '';

  boot = {
    kernelPackages = lib.mkDefault pkgs.linuxPackages_latest;

    tmp = {
      useTmpfs = true;
      tmpfsSize = "100%";
    };

    loader = {
      timeout = lib.mkDefault 1;
      grub.splashImage = null;
    };
  };
  zramSwap = {
    enable = true;
    algorithm = "lz4";
    memoryPercent = 100;
  };

  # show a diff on system profile activation
  system.activationScripts.diff = ''
    if [[ -e /run/current-system ]]; then
      ${pkgs.nix}/bin/nix store diff-closures /run/current-system "$systemConfig"
    fi
  '';

  environment.sessionVariables = {
    # omit S to wrap lines
    SYSTEMD_LESS = "FRXMK";
  };
  environment.systemPackages = with pkgs; [
    wget
    curl
    htop
    iftop
    tmux
    tcpdump
    rsync
    git
    lsof
    screen
    socat
    nmap
    ncdu
    iptables
    pciutils
    usbutils
    alacritty.terminfo
    foot.terminfo
    kitty.terminfo
    rxvt-unicode-unwrapped.terminfo
    jq
  ];

  programs = {
    bash.enableCompletion = true;
    vim.defaultEditor = true;
    zsh.enable = true;
    mtr.enable = true;
    command-not-found.enable = false;
  };

  documentation = {
    doc.enable = false;
    info.enable = false;
  };

  services.journald.extraConfig = ''
    SystemMaxUse=200M
    MaxRetentionSec=2d
  '';

  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = lib.mkDefault false;
      UseDns = false;
    };
  };

  services.fail2ban = {
    enable = lib.mkDefault true;
    ignoreIP =
      # derchris nixos
      ["194.233.171.14/24"]
      # muccc prefixes
      ++ ["83.133.178.0/23" "2001:07F0:3003::/48"]
      # club client prefix
      ++ lib.optionals (config.networking.domain == "club.muc.ccc.de") ["192.168.178.0/23"];

    jails = {
      sshd.settings = {
        enabled = config.services.openssh.enable;
        filter = "sshd[mode=aggressive]";
      };

      nginx-http-auth.settings = {
        enabled = config.services.nginx.enable;
        filter = "nginx-http-auth[mode=aggressive]";
        # shipped failregex doesnt work properly when reading from journald
        failregex = ''\s*\[error\] \d+#\d+: \*\d+ user "(?:[^"]+|.*?)":? (?:password mismatch|was not found in "[^\"]*"), client: <HOST>, server: \S*, request: "\S+ \S+ HTTP/\d+\.\d+", host: "\S+"(?:, referrer: "\S+")?\s*$'';
      };

      dovecot.settings = {
        enabled = config.services.dovecot2.enable;
        filter = "dovecot[mode=aggressive]";
      };

      postfix.settings = {
        enabled = config.services.postfix.enable;
        filter = "postfix[mode=aggressive]";
      };
    };
  };

  users = {
    mutableUsers = false;
    users =
      (selfLib.humanoids.mapUsersByGroup "infra-admins" (_: v: {
        inherit (v) openssh;
        isNormalUser = true;
        extraGroups = ["wheel" "systemd-journal" "docker"];
      }))
      // {
        root = {
          # secrets/credentials.yaml
          hashedPassword = "$y$j9T$Bj/I7HI489OaHV06xw/rJ.$AnFELq9yIKf96vaSuC2oE7m3Jsmu7yql/XFq7rPLC91";
          openssh.authorizedKeys.keys =
            lib.foldlAttrs
            (acc: _: v: acc ++ v.openssh.authorizedKeys.keys)
            []
            (selfLib.humanoids.getUsersByGroup "infra-admins");
        };
      };
  };

  security.sudo = {
    execWheelOnly = lib.mkDefault true;
    wheelNeedsPassword = false;
  };

  # little more prio than mkDefault because this is set by eval-config for currentSystem
  nixpkgs.system = lib.mkOverride 999 "x86_64-linux";

  # include git rev of this repo/flake into the nixos-version
  system.configurationRevision = flakes.self.rev or "dirty";
  system.nixos.revision = flakes.nixpkgs.rev;
  system.nixos.versionSuffix = lib.mkDefault ".${flakes.nixpkgs.shortRev}-${lib.substring 0 8 config.system.configurationRevision}";
  # set nixpkgs on the target to the nixpkgs version of the deployment
  nix.registry.nixpkgs.flake = flakes.nixpkgs;
  nix.nixPath = ["nixpkgs=/run/nixpkgs"];
  systemd.tmpfiles.rules = [
    "L+ /run/nixpkgs - - - - ${flakes.nixpkgs}"
  ];

  security.acme = {
    defaults.email = "noc@muc.ccc.de";
    acceptTerms = true;
  };

  nix.settings = {
    auto-optimise-store = true;
    extra-experimental-features = ["flakes" "nix-command"];
    connect-timeout = 5;
    http-connections = 50;
    trusted-users = ["root" "@wheel"];
    substituters = [
      "https://cache.muc.ccc.de/muccc"
      "https://cache.nixos.org"
      "https://nix-community.cachix.org"
    ];
    trusted-public-keys = [
      "muccc:ATRH5jlmJyn4kpMmnhMVIbEugyr8lsTYvi5KmadQvZk="
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  nix.gc = {
    automatic = true;
    dates = "weekly";
    randomizedDelaySec = "1h";
    options = "--delete-older-than 7d";
  };

  services.prometheus.exporters = {
    node = {
      enable = true;
      enabledCollectors = [
        "systemd"
        "textfile"
        "textfile.directory /run/prometheus-node-exporter"
        (lib.replaceStrings ["\\"] ["\\\\"] "systemd.unit-exclude='.+\\.(automount|device|scope|slice)'")
      ];
      port = 9100;
      openFirewall = true;
    };
    nginx = {
      enable = config.services.nginx.enable && config.services.nginx.statusPage;
      listenAddress = "[::]";
      openFirewall = true;
    };
  };
}
