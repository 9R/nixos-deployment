{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.muccc.multimedia;
in {
  options = {
    muccc.multimedia = {
      enable = lib.mkEnableOption "multimediale unterhaltung";
      username = lib.mkOption {
        type = lib.types.str;
      };
      autostartTmuxp = lib.mkOption {
        type = lib.types.bool;
        default = false;
      };
    };
  };

  config = lib.mkIf cfg.enable {
    services.openssh.settings.PasswordAuthentication = true;

    sound.enable = true;

    hardware.bluetooth = {
      enable = true;
      powerOnBoot = true;
      settings = {
        General = {
          MultiProfile = "multiple";
          FastConnectable = true;
        };
      };
    };

    users = {
      users.${cfg.username} = {
        isNormalUser = true;
        password = "alarm";
        extraGroups = ["wheel" "audio" "video" "dialout"];
        openssh.authorizedKeys.keys =
          config.users.users.root.openssh.authorizedKeys.keys
          ++ [
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFJY+/tAXZFm9U+nJt0kKo6e/TrYiH7E49n0ktbuF5I6 fpletz@fpine"
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN7IyhxIzMjHGq8dBi1jZDYfTBMRm7Z3l+U5ORNveh5e anders@furry"
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG+VGAs4Ym8bF1oNyLpIkSidMJi1kI48uZxQP30VxAA7 tuedel@pinkeye"
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHHvo5N+o0polCh5Yqbg7TOVtWSte6uyyDEk/9IG6AW3 v@x230"
          ];
      };
    };

    home-manager.users.${cfg.username} = {pkgs, ...}: {
      home.stateVersion = "23.05";

      gtk = {
        enable = true;
        theme = {
          name = "Arc-Dark";
          package = pkgs.arc-theme;
        };
        iconTheme = {
          name = "Papirus-Dark";
          package = pkgs.papirus-icon-theme;
        };
        gtk3.extraConfig = {
          gtk-application-prefer-dark-theme = true;
        };
        gtk4.extraConfig = {
          gtk-application-prefer-dark-theme = true;
        };
      };

      qt = {
        enable = true;
        platformTheme = "gtk";
      };

      programs.bash.enable = true;
      programs.tmux = {
        enable = true;
        aggressiveResize = true;
        clock24 = true;
        terminal = "tmux-256color";
        extraConfig = ''
          set -as terminal-features ",gnome*:RGB"
        '';
      };
      programs.ncmpcpp.enable = true;
      programs.foot = {
        enable = true;
        settings = {
          main = {
            font = "Fira Code:size=18";
          };
        };
      };
      services.mpd = {
        enable = true;
        musicDirectory = "/home/${cfg.username}";
      };
      wayland.windowManager.sway = {
        enable = true;
        wrapperFeatures.gtk = true;
        config = {
          modifier = "Mod4";
          bars = [];
          terminal = "foot";
          window = {
            titlebar = false;
            hideEdgeBorders = "smart";
          };
          floating = {
            titlebar = false;
          };
          startup = lib.mkIf cfg.autostartTmuxp [
            {command = "${pkgs.foot}/bin/foot tmuxp load -y start";}
          ];
        };
      };
      programs.waybar = {
        enable = true;
        systemd.enable = true;
      };
      programs.mpv = {
        enable = true;
        config = {
          vo = "gpu";
          gpu-context = "wayland";
          ytdl-format = "bestvideo[ext=webm][height<=?1080]+bestaudio[acodec=opus]/bestvideo[height<=?1080]+bestaudio/best";
        };
      };

      home.packages = with pkgs; [
        youtube-dl
        pulsemixer
        cmatrix
        tmuxp
        screen
        cli-visualizer
        playerctl
      ];

      services.spotifyd = {
        enable = true;
        package = pkgs.spotifyd.override {
          withALSA = false;
          withPortAudio = false;
          withMpris = true;
        };
        settings.global = {
          device_name = config.networking.hostName;
          backend = "pulseaudio";
          bitrate = 320;
          device_type = "computer";
        };
      };

      programs.librewolf = {
        enable = true;
        package = pkgs.librewolf-wayland.override (attrs: {
          extraPrefs = ''
            pref("ui.systemUsesDarkTheme", 1);
            pref("privacy.webrtc.legacyGlobalIndicator", false);
            pref("media.ffmpeg.vaapi.enabled", true);
            pref("gfx.webrender.enabled", true);
            pref("gfx.webrender.all", true);
            pref("gfx.webrender.compositor", true);
            pref("gfx.webrender.compositor.force-enabled", true);
            pref("webgl.disabled", false);
            pref("privacy.resistFingerprinting", false);
            pref("network.dns.echconfig.enabled", true);
            pref("network.dns.http3_echconfig.enabled", true);
            pref("geo.enabled", false);
          '';
        });
      };
    };

    services.pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
    };
    security.rtkit.enable = true;

    programs.sway.enable = true;
    fonts.fonts = with pkgs; [fira fira-code fira-code-symbols fira-mono powerline-fonts font-awesome_4 font-awesome_5];

    services.greetd = {
      enable = true;
      settings = {
        initial_session = {
          command = "sway";
          user = cfg.username;
        };
        default_session = {
          command = "${lib.makeBinPath [pkgs.greetd.tuigreet]}/tuigreet --time --cmd sway";
          user = "greeter";
        };
      };
    };

    services.resolved = {
      llmnr = "false"; # we use avahi instead
    };
    services.avahi = {
      enable = true;
      publish = {
        enable = true;
        addresses = true;
        domain = true;
        hinfo = true;
        userServices = true;
      };
    };

    systemd.user.services.shairport-sync = {
      wantedBy = ["default.target"];
      serviceConfig = {
        ExecStart = "${pkgs.shairport-sync}/bin/shairport-sync -v -o pa";
        Restart = "always";
      };
    };

    systemd.user.services.bt-agent-a2dp = {
      description = "A2DP Bluetooth Agent";
      wantedBy = ["default.target"];
      bindsTo = ["dbus.service"];
      environment.DBUS_SESSION_BUS_ADDRESS = "unix:path=/run/user/1000/bus";
      serviceConfig = {
        ExecStartPre = "${pkgs.bluez}/bin/bluetoothctl discoverable on";
        ExecStart =
          pkgs.writers.writePython3 "a2dp-agent"
          {libraries = with pkgs.python3Packages; [dbus-python pygobject3];}
          (builtins.readFile ../static/multimedia/a2dp-agent.py);
      };
    };
  };
}
