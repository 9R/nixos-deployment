# muCCC NixOS Deployment

This repository is a Nix flake and provides a development shell with all required
tools. Use `nix develop` to drop into a shell or just use `direnv` with
`nix-direnv`.

## Tools used

 * https://github.com/zhaofengli/colmena
 * https://github.com/Mic92/sops-nix

## Common Tasks

### Deploy a host

```
colmena apply -v --on briafzentrum
colmena apply -v --build-on-target --on loungepi
```

### Update input flakes

```
nix flake update --commit-lock-file
```

### Check if all outputs evaluate

```
nix flake check
```

### Build images

```
nix build .\#proxmoxImages.base
nix build .\#proxmoxImages.briafzentrum
nix build .\#sdImages.hauptraumpi
```

### Apply a configuration without colmena

```
nixos-rebuild switch --flake .\#auth
nixos-rebuild switch --flake .\#loungepi --target-host root@loungepi.club.muc.ccc.de
```

### Reencrypt all sops secrets after key changes

```
nix run .\#sops-updatekeys
```
