{
  self,
  formats,
}:
(formats.yaml {}).generate "sops.yaml" {
  creation_rules =
    [
      {
        path_regex = "secrets/credentials\.yaml$";
        key_groups = [
          {
            age = self.lib.humanoids.getAgeForGroup "infra-admins";
          }
        ];
      }
    ]
    ++ self.lib.hosts.mapAllHostsToList (
      host: values: {
        path_regex = "secrets/${host}(/[^/]+)?\.yaml$";
        key_groups = [
          {
            age =
              [values.age]
              ++ self.lib.humanoids.getAgeForGroup "infra-admins";
          }
        ];
      }
    );
}
