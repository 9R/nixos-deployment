{
  writers,
  sops,
  curl,
}:
writers.writeBashBin "issue-after-flake-update" ''
  ALL_HOSTS=$(for hostfn in `ls hosts/*.nix | cut -f 1 -d '.' | awk -F '/' '{print $2}'`; do echo -n "- [] "; echo -n "$hostfn<br>"; done)
  API_TOKEN=$(${sops}/bin/sops -d --extract '["gitea_api"]' secrets/credentials.yaml)
  ${curl}/bin/curl \
      'https://gitea.muc.ccc.de/api/v1/repos/muCCC/nixos-deployment/issues?token='$API_TOKEN \
      -H 'accept: application/json' \
      -H 'Content-Type: application/json' \
      -d "{
      \"title\": \"Host update after latest flake update\",
      \"body\": \"Update the following hosts:<br><br>$ALL_HOSTS\"
      }"
''
