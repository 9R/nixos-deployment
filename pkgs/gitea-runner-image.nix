{
  system,
  inputs,
  dockerTools,
  buildEnv,
  coreutils-full,
  bashInteractive,
  nix,
  cacert,
  gitMinimal,
  gnutar,
  gzip,
  openssh,
  xz,
  iana-etc,
  writeTextFile,
  nodejs_20,
  attic-client,
}:
dockerTools.buildImageWithNixDb {
  name = "nix";
  uid = 0;

  copyToRoot = buildEnv {
    name = "nix";
    ignoreCollisions = true;
    pathsToLink = ["/bin" "/etc"];
    paths = [
      "${inputs.docker-nixpkgs}/images/nix/root"
      coreutils-full
      bashInteractive
      nix
      cacert
      gitMinimal
      gnutar
      gzip
      openssh
      xz
      iana-etc
      (writeTextFile {
        name = "nix.conf";
        destination = "/etc/nix/nix.conf";
        text = ''
          accept-flake-config = true
          experimental-features = nix-command flakes
        '';
      })

      inputs.nixpkgs
      nodejs_20
      attic-client
      inputs.nix-fast-build.packages.${system}.default
    ];
  };

  extraCommands = ''
    mkdir root
    mkdir usr
    ln -s ../bin usr/bin
    mkdir -p -m 1777 tmp var/tmp
  '';

  config = {
    Cmd = ["/bin/bash"];
    Env = [
      "NIX_PATH=nixpkgs=${inputs.nixpkgs}"
      "PAGER=cat"
      "PATH=/usr/bin:/bin"
      "SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt"
      "USER=root"
    ];
  };
}
